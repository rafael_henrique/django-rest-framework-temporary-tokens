# Temporary Tokens for the Django REST Framework  #
This package extends Django REST Frameworks so that authentication tokens are set to expire after an amount of time specified in settings.py. This is useful if you want to make sure your authentication token expires after a set number of minutes and, optionally, allow the token to be refreshed for the same number of minutes upon every successful use of the token. This behavior is more similar to a session where authentication is valid after supplying a username and password and is refreshed to extend the session each time a request is made.

The current behavior of Django REST Framework using [token-based authentication](http://www.django-rest-framework.org/api-guide/authentication/#tokenauthentication) is to create a token that never expires.

## Installation ##

This module has been tested in Django 1.8 and 1.9 and should work in other versions. It should work with Django REST Framework 3.2.3 and above.

Install via pip:

```
#!python

pip install django-rest-framework-temporary-tokens
```

This package extends [TokenAuthentication](http://www.django-rest-framework.org/api-guide/authentication/#tokenauthentication) so you should get that working first.

If the above is working properly, go ahead and out this package to INSTALLED_APPS right under 'rest_framework':


```
#!python

INSTALLED_APPS = [
    ...
    'rest_framework',
    'rest_framework_temporary_tokens',
    ...
]
```

Modify your authentication class like this:


```
#!python

REST_FRAMEWORK = {
    ...
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_temporary_tokens.authentication.TemporaryTokenAuthentication',
    ),
    ...
}
```

Add the following to settings.py:

```
#!python

REST_FRAMEWORK_TEMPORARY_TOKENS = {
    'MINUTES': 30,
    'RENEW_ON_SUCCESS': True,
    'USE_AUTHENTICATION_BACKENDS': False,
}
```
You can set the number of minutes to any integer you want. Set RENEW_ON_SUCCESS to False if you want the token to ONLY be good from the time it is set. Leaving it as True will 'refresh' the expiration time on the token every time it is successfully used. Set USE_AUTHENTICATION_BACKENDS to True if you are using some other authentication backend, such as those provided by [django-allauth](http://www.intenct.nl/projects/django-allauth/).

At this point, you will want to run a migration.
```
#!shell

./manage.py migrate rest_framework_temporary_tokens
```


If you used the obtain_auth_token view, you will need to update your urls.py to use this module:


```
#!python
...
from rest_framework_temporary_tokens import views as authtoken_views
...
urlpatterns = [
    ...
    url(r'^api-token-auth/', authtoken_views.obtain_temporary_auth_token),
    ...
]

```

If you are assigning them some other way, you can create one like this:

```
#!python

from rest_framework_temporary_tokens.models import TemporaryToken

token = TemporaryToken.objects.create(user=...)
print token.key
```
If the user has an existing token, you will need to delete it first.

You can manually expire a token like this:

```
#!python

from rest_framework_temporary_tokens.models import TemporaryToken

token = TemporaryToken.objects.get(user=...)
token.expire()

```
This will not delete the token.